package com.company;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Scanner;
public class pemesanan {
    public static void main(String args[])
    {
        //Inisialisasi objek untuk masing-masing kelas
        Scanner input = new Scanner(System.in);
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        //Deklarasi variabel
        int jum=0, i=0;
        double total_bayar=0;
        //Deklarasi array
        int [] kode = new int[5];
        int [] qty = new int[5];
        int [] harga = new int[5];
        double [] sub_total = new double[5];
        double [] diskon= new double [5];
        String [] barang = new String[5];

        System.out.println("PROGRAM PEMESANAN KAOS\n");

        System.out.println("==============================");
        System.out.println("|| KD 1                     ||");
        System.out.println("|| _________________________||");
        System.out.println("|| KAOS LEVIS               ||");
        System.out.println("|| BAHAN COTTON             ||");
        System.out.println("|| Harga Rp.50000           ||");
        System.out.println("|| TANGGAL JADI 01-02-2021  ||");
        System.out.println("==============================\n\n");

        System.out.println("==============================");
        System.out.println("|| KD 2                     ||");
        System.out.println("|| _________________________||");
        System.out.println("|| KAOS ADIDAS              ||");
        System.out.println("|| BAHAN COTTON             ||");
        System.out.println("|| Harga Rp.70000           ||");
        System.out.println("|| TANGGAL JADI 02-02-2021  ||");
        System.out.println("==============================\n\n");

        System.out.println("==============================");
        System.out.println("|| KD 3                     ||");
        System.out.println("|| _________________________||");
        System.out.println("|| KAOS NIKE                ||");
        System.out.println("|| BAHAN COTTON             ||");
        System.out.println("|| Harga Rp.60000           ||");
        System.out.println("|| TANGGAL JADI 03-02-2021  ||");
        System.out.println("========================\n\n");

        System.out.println("==============================");
        System.out.println("|| KD 4                     ||");
        System.out.println("|| _________________________||");
        System.out.println("|| KAOS VANS                ||");
        System.out.println("|| BAHAN POLYSTER           ||");
        System.out.println("|| Harga Rp.55000           ||");
        System.out.println("|| TANGGAL JADI 03-02-2021  ||");
        System.out.println("========================\n\n");

        System.out.println("==============================");
        System.out.println("|| KD 5                      ||");
        System.out.println("|| _________________________ ||");
        System.out.println("|| KAOS GUCCI                ||");
        System.out.println("|| BAHAN SUTRA               ||");
        System.out.println("|| Harga Rp.200000           ||");
        System.out.println("|| TANGGAL JADI 03-02-2021   ||");
        System.out.println("========================\n\n");

        System.out.print("Masukan Jumlah Beli : ");
        jum=input.nextInt();
        System.out.println(" ");
        //Memasukan elemen didalam array
        for (i=0; i<jum;i++){
            System.out.print("Masukan Kode Barang Ke-"+(i+1)+" : ");
            kode[i]=input.nextInt();
            System.out.print("Masukan qty Ke-"+(i+1)+" : ");
            qty[i]=input.nextInt();
            //Menentukan barang berdasarkan kode yang dimmasukan
            switch (kode[i]){
                case 1 :
                    barang[i]="Kaos Levis  ";
                    harga[i]=50000;
                    diskon[i]=0.1;
                    break;
                case 2 :
                    barang[i]="Kaos Adidas ";
                    harga[i]=70000;
                    diskon[i]=0.05;
                    break;
                case 3 :
                    barang[i]="Kaos Nike";
                    harga[i]=60000;
                    diskon[i]=0.5;
                    break;
                case 4 :
                    barang[i]="Kaos Vans";
                    harga[i]=55000;
                    diskon[i]=0.3;
                    break;
                case 5 :
                    barang[i]="Kaos GUCCI";
                    harga[i]=200000;
                    diskon[i]=0;
                    break;
                default :
                    System.out.println("Kode Barang Tidak Tersedia");
            }
        }
        //Pengaturan format number
        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        kursIndonesia.setDecimalFormatSymbols(formatRp);

        System.out.println(" ");
        System.out.println("No   Nama Barang         Harga        QTY  Diskon   Sub Total");
        //Menampilkan seluruh elemen di dalam array
        for (i=0; i<jum;i++){
            sub_total[i]=((qty[i]*harga[i])-(qty[i]*harga[i]*diskon[i]));
            total_bayar+=sub_total[i];
            System.out.println(i+1+"    "+barang[i]+"   "+kursIndonesia.format(harga[i])+"    "+qty[i]+"     "+(int)(diskon[i]*100)+"%"+"     "+kursIndonesia.format(sub_total[i]));
        }
        System.out.println(" ");
        //Menampilkan total bayar
        System.out.println("Total Bayar : "+kursIndonesia.format(total_bayar));
        System.out.println("No rek. 2015018296, A/N Dicky Rizky P");
    }
}